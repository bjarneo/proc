<?php
/**
 * This Class I'll get the information from Proc.
 * Author: Bjarne Øverli
 * Website: http://www.codephun.com
 */

class Proc {

    public function __construct()
    {

    }

    public function init()
    {

    }

    public function getVersionSignature()
    {
        $fileInfo = $this->_getDirectory();

        $fh = fopen($fileInfo['version_signature'], 'r');
        $info = array();

        while($line = fgets($fh)) {
            $info[] = $line;
        }


        return $info;
    }

    public function getMemInfo()
    {
        return $this->getFileInfo('meminfo');
    }
    /**
     * Public function to get our file information
     * @param bool $filename
     * @return array|bool
     */
    public function getFileInfo($filename = false)
    {
        $fileInfo = $this->_getDirectory();

        if($filename) {
            return $this->_sortFileToArray($fileInfo[$filename]);
        }

        return false;
    }

    /**
     * Pass filename and return information as array
     * @param $file
     * @return array $info
     */
    protected function _sortFileToArray($file)
    {
        $fh = fopen($file, 'r');
        $info = array();
        while($line = fgets($fh)) {
            $fexplode = explode(':', $line);
            $info[str_replace(' ', '', (string)$fexplode[0])] = str_replace(' ', '', (string)$fexplode[1]);
        }

        return $info;
    }

    /**
     * Get files from proc folder
     * @return $this array
     */
    protected function _getDirectory()
    {
        $files = array();
        $dir = new DirectoryIterator('/proc');

        foreach($dir as $file) {
            if($file->isFile()) {
                $files[$file->getFilename()] = $file->getPathname();
            }
        }

        return $files;
    }
}