<?php $this->load->view('header.php'); ?>
<?php
$attributes = array(
    'class' => 'coditor_form',
    'id' => 'coditor'
);
echo form_open('', $attributes);
echo form_input(
    array(
        'name' => 'title',
        'placeholder' => 'Code snippet name',
        'class' => 'coditor_title'
    )
);
echo form_textarea(
    array(
        'name' => 'code',
        'class' => 'coditor',
        'id' => 'coditor_code',
        'spellcheck' => false
    )
);
?>
<button type="button" id="save-file" class="btn btn-primary">Save file</button>
<button type="button" id="publish-gist" class="btn btn-default">Publish as GIST</button>
<?php $this->load->view('footer.php'); ?>