<h1>Meminfo</h1>

<div id="dashboard"></div>

<script id='dashboardTemplate' type='text/ractive'>
    <table class="table memtable">
        <thead>
        <tr>
            <th>#</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        {{#meminfo:i}}
        <tr>
            <td>{{i}}</td>
            <td>{{.}}</td>
        </tr>
        {{/meminfo}}
        </tbody>
    </table>
</script>
<script src="<?php echo base_url('/assets/js/memory.js'); ?>"></script>