<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('/assets/images/favicon.png'); ?>">
    <title><?php echo $meta_title; ?></title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('/assets/css/bootstrap.css'); ?>" rel="stylesheet">
    <!-- App css -->
    <link href="<?php echo base_url('/assets/css/proc.css'); ?>" rel="stylesheet">
    <!-- jQuery google cdn -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo base_url('/assets/js/ractive.min.js'); ?>"></script>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Proc UI</a>
    </div>
    <p class="navbar-text pull-right"><?php //printf("Version: %s", $version[0]) ?></p>
</nav>
<div id="container"> <!-- start container -->
    <div class="dashboard-menu">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="<?php echo base_url('index.php/'); ?>"><span class="glyphicon glyphicon-home left space-right"></span>Dashboard<span class="glyphicon glyphicon-circle-arrow-right right"></span></a></li>
            <li><a href="<?php echo base_url('index.php/dashboard/memory/'); ?>"><span class="glyphicon glyphicon-stats left space-right"></span>Memory<span class="glyphicon glyphicon-circle-arrow-right right"></span></a></li>
            <li><a href="<?php echo base_url('index.php/dashboard/cpu/'); ?>"><span class="glyphicon glyphicon-stats left space-right"></span>CPU<span class="glyphicon glyphicon-circle-arrow-right right"></span></a></li>
            <li><a href="<?php echo base_url('index.php/dashboard/apache/'); ?>"><span class="glyphicon glyphicon-stats left space-right"></span>Apache<span class="glyphicon glyphicon-circle-arrow-right right"></span></a></li>
            <li><a href="<?php echo base_url('index.php/dashboard/mysql/'); ?>"><span class="glyphicon glyphicon-stats left space-right"></span>MySQL<span class="glyphicon glyphicon-circle-arrow-right right"></span></a></li>
            <li><a href="<?php echo base_url('index.php/coditor/'); ?>"><span class="glyphicon glyphicon-stats left space-right"></span>Coditor<span class="glyphicon glyphicon-circle-arrow-right right"></span></a></li>
        </ul>
    </div>
    <div class="content"> <!-- start content -->
