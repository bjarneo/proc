<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Coditor is to create, save and update code snippets.
 * Author: Bjarne Øverli
 * Website: http://www.codephun.com
 */

//todo http://alexgorbatchev.com/SyntaxHighlighter/manual/installation.html
class Coditor extends CI_Controller {

    // Directory for our files
    protected $_dir;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // Create our directory if not exists
        if(!$this->_createDirectory()) {
            exit('Directory couldn\'t be created. Please set the permission of assets folder to 777');
        }

        $data = array(
            'meta_title' => 'Proc'
        );
        $this->load->view('coditor', $data);
    }

    public function saveFile()
    {
        $post = $this->input->post(NULL, TRUE);

        return json_encode(array('post' => $post));
    }

    public function editFile()
    {

    }

    protected function _listFiles()
    {

    }

    protected function _createDirectory()
    {
        $this->_dir = FCPATH . 'assets/coditor/';
        if(!is_dir($this->_dir)) {
            mkdir($this->_dir, 0777);
        }

        return true;
    }

}