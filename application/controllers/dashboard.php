<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Dashboard.
 * Author: Bjarne Øverli
 * Website: http://www.codephun.com
 */

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    }

    public function index()
    {
        $data = array(
            'meta_title' => 'bjarneo@codephun:/proc$',
            'version' => $this->proc->getVersionSignature(),
            'meminfo' => $this->proc->getMemInfo(),
        );

        $this->_view('dashboard', $data);
    }

    public function memory()
    {
        $data = array(
            'meta_title' => 'bjarneo@codephun:/proc$',
            'version' => $this->proc->getVersionSignature()
        );

        $this->_view('meminfo', $data);
    }
    public function proc()
    {
        echo '<pre>';
        print_r($this->proc->getFileInfo('meminfo'));
        echo '</pre>';
    }

    public function getMemInfo()
    {
        header('Content-type: application/json');
        echo json_encode($this->proc->getFileInfo('meminfo'), true);
    }

    /**
     * Just a view loader
     * @param string $content
     * @param array $data
     */
    protected function _view($content, $data = false)
    {
        ($data) ? $this->load->view('header.php', $data) : $this->load->view('header.php');
        ($data) ? $this->load->view($content, $data) : $this->load->view($content);
        ($data) ? $this->load->view('footer.php', $data) : $this->load->view('footer.php');
    }
}
