/**
 *  Update Memory information each second
 *  Just half finished code
 */

var Memory = {
    init: function () {
        this.dashboard = new Ractive({
            el: 'dashboard',
            template: '#dashboardTemplate',
            data: {}
        });

        this.updateMem();
        setInterval(this.updateMem, 1000);
    },

    updateMem: function () {
        var self = Memory;
        $.getJSON('/proc/index.php/dashboard/getMemInfo/').then(function (data) {
            self.dashboard.set({
                meminfo: data
            });
        });
    }

};
Memory.init();