/**
 *
 * Proc JS
 * Author: Bjarne Øverli
 * Website: http://www.codephun.com
 *
 * Currently not in use (except menuActive)
 */


var Proc =
{
    // Set the Web application base URL.
    baseUrl: window.location.href,

    init: function() {
        $().click(function(){});
        Proc.menuActive();
    },

    saveFile: function() {
        var title = $(".coditor_title").val(),
            code =  $("#coditor_code").val();
        var postData = {
            'title' : title,
            'code' : code
        };
        console.log(postData);
        $.post(Proc.baseUrl + '/coditor/savefile/', postData, function(data){
            console.log(data);
        });
    },

    // Quick menu hack. Can be done in php
    menuActive: function() {
        var menu = $('.dashboard-menu ul li'),
            len = menu.length,
            i = 0,
            j = 0;

        for(; i < len; i++) {
            var menuEle = $(menu[i]);
            if(menuEle.find('a').attr('href') === window.location.href) {
                menuEle.addClass('active');
                for(; j < len; j++) {
                    var menuEle = $(menu[j]);
                    if(menuEle.find('a').attr('href') !== window.location.href) {
                        menuEle.removeClass('active');
                    }
                }
            }
        }
    }
};

Proc.init();